#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10001

typedef enum type  {
    DATA, ACK, NACK
} type_t;

typedef struct mys {
    int type;
    int noSeq;
    char data[1400-2*sizeof(int)];
} my_t;


int main(void)
{
	msg r;
	int i, res, noSeq = 0;
	my_t message;
	
	printf("[RECEIVER] Starting.\n");
	init(HOST, PORT);
	
	for (i = 0; i < COUNT; i++) {
		/* wait for message */
		res = recv_message(&r);
		memcpy(&message, r.payload, 1400);
		if (res < 0) {
			perror("[RECEIVER] Receive error. Exiting.\n");
			return -1;
		}
		printf("[RECV] received %i\n", message.noSeq);		
		/* send dummy ACK */
		if(message.noSeq == noSeq) {
			message.type = ACK;
			memcpy(r.payload, &message, 1400);
			noSeq++;
		} else {
			message.type = NACK;
			message.noSeq = noSeq;
			memcpy(r.payload, &message, 1400);
		}

		res = send_message(&r);
		if (res < 0) {
			perror("[RECEIVER] Send ACK error. Exiting.\n");
			return -1;
		}
	}

	printf("[RECEIVER] Finished receiving..\n");
	return 0;
}
