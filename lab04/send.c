#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10000

typedef enum type  {
	DATA, ACK, NACK
} type_t;

typedef struct mys {
	int type;
	int noSeq;
	char data[1400-2*sizeof(int)];
} my_t;

int main(int argc, char *argv[])
{
	my_t message;
	msg t;
	int i, res, noSeq;
	int N, poz = 0;
	char** buff;

	N = atoi(argv[1]) * 1000 / 8 / 1400;
	printf("N = %i\n", N);
	buff = (char**)calloc(N, sizeof(char*));

	printf("[SENDER] Starting.\n");	
	init(HOST, PORT);

	/* printf("[SENDER]: BDP=%d\n", atoi(argv[1])); */
	for(i = 0; i < N; i++)	
		buff[i] = (char*)calloc(1400, sizeof(char));
	
	noSeq = 0;
	for(i = 0; i < N; i++) {
		message.type = DATA;
		message.noSeq = noSeq++;
		sprintf(message.data, "Hello World of PC!");
		memcpy(buff[i], &message, sizeof(message));
	}

	for(i = 0; i < N; i++) {
		t.len = 1400;
		memcpy(t.payload, buff[i], 1400);
		send_message(&t);
	}

	while( i < COUNT) {
		
		/* wait for ACK */
		res = recv_message(&t);
		if (res < 0) {
			perror("[SENDER] Receive error. Exiting.\n");
			return -1;
		}

		memcpy(&message, t.payload, 1400);
		if(message.type == ACK) {
			message.type = DATA;
			message.noSeq = noSeq++;
			sprintf(message.data, "Hello World of PC!");
			memcpy(buff[poz], &message, sizeof(message));
			memcpy(t.payload, &message, 1400);
			poz = (poz + 1) % N;
			i++;
		} else {
			poz = message.noSeq % N;
			noSeq = message.noSeq + 1;
			message.type = DATA;
			memcpy(buff[poz], &message, sizeof(message));
			poz = (poz + 1) % N;
		} 

		/* send msg */
		res = send_message(&t);
		if (res < 0) {
			perror("[SENDER] Send error. Exiting.\n");
			return -1;
		}

	}

	printf("[SENDER] Job done, all sent.\n");
	for(i = 0; i < N; i++)
		free(buff[i]);
	free(buff);	
	buff = NULL;
	return 0;
}
