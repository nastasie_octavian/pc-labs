#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10001

int main(int argc,char** argv){
  init(HOST,PORT);
  msg r,t;
  // total_size = dimensiunea totala a fisierului de primit
  // current_size = nr de bytes deja primiti
  int total_size, current_size = 0, ack, fd_write;

  // deschid fisierul pt scriere, obtin file descriptor
  fd_write = open("input.txt.recv", O_WRONLY | O_CREAT, 00644);
  // daca a avut loc o eroare, semnalez, inchid fisierul si inchid programul
  if(fd_write < 0) {
    perror("Erorr opening the file");
	close(fd_write);
  	return -1;
  }

  // astept sa primesc mesajul initial cu lungimea fisierului de transmis
  if (recv_message(&r)<0){
    perror("Receive message");
	close(fd_write);
    return -1;
  }
  
  // afisiez numele si dimensiunea fisierului de primit
  total_size = r.len;
  ack = r.payload[0];
  
  printf("[%s] Se primeste fisierul \"%s\" de dimensiune %i bytes\n", 
  	argv[0], r.payload + 1, total_size);

  // construiesc ack-ul de confirmare
  t.len = 1;
  t.payload[0] = ack;
  send_message(&t);
 
  // cat timp mai am de primit bytes din fisier
  while(current_size < total_size) {
    // astept sa primesc
  	if(recv_message(&r) < 0) {
		perror("Error receiving the file");
		close(fd_write);
		return -1;
	}

	// actualizez nr de bytes primiti
	current_size += r.len - 1;
	ack = r.payload[0];

    // scriu in fisier datele primite din payload + 1 (la payload[0] se afla ack)
	if(write(fd_write, r.payload + 1, r.len - 1) < 0) {
		perror("Error writing file");
		close(fd_write);
		return -1;
	}

  	printf("[%s] S-au primit %i bytes cu ACK: %i\n", argv[0], r.len - 1, ack);

	// construiesc mesajul de ack
	t.len = 1;
	t.payload[0] = ack;
	send_message(&t);
  }

  close(fd_write);
  return 0;
}
