#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10000
#define DATA_SIZE 200

int main(int argc,char** argv){
  init(HOST,PORT);
  msg t;
  // buffer pt citit datele din fisier, dimensiunea este data de DATA_SIZE
  char buff[DATA_SIZE];
  struct stat s;
  int size, fd_open, ack = 0;
  
  // deschid fisierul de input pt citire, obtin file descriptor
  fd_open = open("input.txt", O_RDONLY);

  // daca a avut loc o eroare, atunci semnalez, inchid fisierul si inchid programul
  if(fd_open < 0) {
  	perror("Error opening the file");
	close(fd_open);
	return -1;
  }

  // obtin dimensiunea fisierului
  stat("input.txt", &s);
  size = s.st_size;

  // prima data trimit dimensiunea fisirului in len
  // ack 0 in payload[0] si numele fisierului in payload + 1
  sprintf(t.payload + 1, "input.txt");
  t.len = size;
  t.payload[0] = ack;
  // trimit mesajul
  send_message(&t);

  // astept sa primesc ack ca a fost primit mesajul
  if (recv_message(&t) < 0){
    perror("Receive error");
    close(fd_open);
	return -1;
  }
  else {		

    // verific sa fie ack-ul correct
  	if(ack != t.payload[0]) {
			perror("Wrong ACK");
  			close(fd_open);
			return -1;
	}
    printf("[%s] Got reply with ACK: %i\n", argv[0], t.payload[0]);

	// cat timp nu am terminat de trimis fisierul, incerc sa citesc 
	// DATA_SIZE bytes in buff, nr de bytes citit efectiv este size
	while((size = read(fd_open, buff, DATA_SIZE)) > 0) {
	    // trec la urmatoare valoare pt ack adunare cu 1 in modulo 2, eg 0 -> 1; 1 -> 0
		ack = (ack + 1) % 2;
		// construiesc mesajul de transmis, len nr de bytes cititi + 1 pt ack
		t.len = size + 1;
		t.payload[0] = ack;
		// cpiez datele din buff in payload
		memcpy(t.payload + 1, buff, size);
        // trimit mesajul
		send_message(&t);

        // astept raspuns de la recv, si verific daca ACK-ul este corect
		if((recv_message(&t)) < 0) {
			perror("Error receiving ACK");
			close(fd_open);
			return -1;
		}
		
		if(ack != t.payload[0]) {
			perror("Wrong ACK");
  			close(fd_open);
			return -1;
		}

        printf("[%s] Got reply with ACK: %i\n",argv[0],t.payload[0]);
	}
  }

  close(fd_open);
  return 0;
}
