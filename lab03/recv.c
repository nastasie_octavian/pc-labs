#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10001

void setBit(char* control, int poz, int val) {
    *control = *control | (val << poz);
}

int getBit(char control, int poz) {
    return (control & (1 << poz)) >> poz;
}

char getHamm(char octet) {
    int i;
	char r = 0;
    for(i = 0; i < 8; i++)
        r = r ^ getBit(octet, i);
    return r;
}

int main(int argc,char** argv){
  msg r,t;
  init(HOST,PORT);

  int len, i;
  char control;
  if (recv_message(&r)<0){
    perror("Receive message");
    return -1;
  }

  len = r.len;
  control = r.payload[len-1];
  printf("[%s] Control: %i\n", argv[0], control);
  r.payload[len-1] = '\0';
  for(i = 0; i < len-1; i++) {
	if(getHamm(r.payload[i]) != getBit(control, i))
		printf("[%s] Eroare, pozitia %i \"%c\" (%i != %i)\n", 
			argv[0], i, r.payload[i], getHamm(r.payload[i]), getBit(control, i));
  }
  printf("[%s] Got msg with payload: %s\n",argv[0],r.payload);

  sprintf(t.payload,"ACK(%s)",r.payload);
  t.len = strlen(t.payload+1);
  send_message(&t);
  return 0;
}
