#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10000

void setBit(char* control, int poz, int val) {
	*control = *control | (val << poz);
}

int getBit(char control, int poz) {
	return (control & (1 << poz)) >> poz;
}

char getHamm(char octet) {
	int i; 
	char r = 0;
	for(i = 0; i < 8; i++)
		r = r ^ getBit(octet, i);
	return r;
}

int main(int argc,char** argv){
  init(HOST,PORT);
  msg t;

  char *data = "Hello World of PC";
  char control = 0;
  int i;
  for(i = 0; i < 8; i++)
  	setBit(&control, i, getHamm(data[i]));
	
  memcpy(t.payload, data, 8);
  memcpy(t.payload + 8, &control, 1);
  t.len = 9;
  printf("[%s] Control: %i\n", argv[0], control);
  send_message(&t);

  if (recv_message(&t)<0){
    perror("receive error");
  }
  else {
    printf("[%s] Got reply with payload: %s\n",argv[0],t.payload);
  }

  return 0;
}
